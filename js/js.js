function generarId(length) {
    var id = '';
    var caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var caracteresLength = caracteres.length;
    for (var i = 0; i < length; i++) {
        id += caracteres.charAt(Math.floor(Math.random() * caracteresLength));
    }
    return id;
}
//document.write(generarId(20));


class Nota {

    constructor(titulo, cont) {
        this.titulo = titulo;
        this.cont = cont;
        this.id = generarId(20);
        this.modificacion = new Date();
    }

    set setTitulo(titulo) {
        this.titulo = titulo;
    }

    get getTitulo() {
        return this.titulo;
    }

    set setCont(cont) {
        this.cont = cont;
    }

    get getCont() {
        return this.cont;
    }
}


function agregarNota() {
    let titulo = document.getElementById('titulo');
    let cont = document.getElementById('cont');
   
   
    if (titulo.value != "" && cont.value != "") {
        let nuevaNota = new Nota(
            titulo.value,
            cont.value
        );

        if(existeNota(nuevaNota)){
            alert('Ya existe ese titulo');
        } else {
            let notas = JSON.parse(localStorage.getItem('notas'));
            if (!notas) {
                notas = [];
            }
            notas.push(nuevaNota);
            localStorage.setItem('notas', JSON.stringify(notas));
    

            titulo.value = "";
            cont.value = "";
            alert('Nota guardada correctamente');
        }

    } else {
        alert('Complete los campos');
    }
}


function listarNotas(notas = null) {
    if(notas == null){
        notas = JSON.parse(localStorage.getItem('notas'));
        
        if (!notas) {
            notas = [];
        }
    }


    let info = "";
    for (let index = 0; index < notas.length; index++) {
        let mostrar = notas[index];
        info += '<div class="card mb-3" style="max-width: 540px;"><div class="row no-gutters"><div class="col-md-3"><img src="img/limon.jpeg" class="card-img" alt="..."></div><div class="col-md-8"><div class="card-body"><p class="card-text"> ID : ' + mostrar.id +'<br> Titulo : '+ mostrar.titulo +'<br> Contenido : '+ mostrar.cont + '<br><br> Creado : ' + mostrar.modificacion +'<br><br>';
        info += '<button type="button" class="btn btn-warning btn-sm mr-1" data-toggle="modal" data-target="#editModal" onclick="cargarFormMod(\''+ mostrar.titulo +'\')"><i class="fas fa-user-edit"></i></button>';
        info += '<button type="button" class="btn btn-danger btn-sm" onclick="eliminarNotas(\''+mostrar.titulo+'\')"><i class="fas fa-trash-alt"></i></button></p> <p class="card-text"><small class="text-muted"></small></p></div></div></div></div>';
        
    }

    document.getElementById('tablaNotas').innerHTML = info;
    
}

function eliminarNotas(titulo) {
    if(confirm('¿Esta seguro de borrar la nota?')){
        let notas = JSON.parse(localStorage.getItem('notas'));
        let indice = notas.findIndex(cont => cont.titulo == titulo);
        notas.splice(indice,1);
        localStorage.setItem('notas', JSON.stringify(notas));
        alert('Nota eliminada');
        
        buscar();
    }
}

function existeNota(contact) {
    let notas = JSON.parse(localStorage.getItem('notas'));
    if(notas){
        return notas.find(mostrar => mostrar.titulo == contact.titulo) != null;
    } else {
        return false;
    }
}

function buscar() {
    console.log(event);
    if (event.type == "submit") {
        event.preventDefault();
    }
    let txt = document.getElementById('txt-buscador').value;
    let notas = JSON.parse(localStorage.getItem('notas'));
    // Controlo que se haya ingresado algo en el buscador
    // y que la notas no este vacía
    if (txt && notas) {
        let notasFiltrada = notas.filter(contacto => contacto.titulo.toLowerCase().indexOf(txt.toLowerCase()) > -1 );
        listarNotas(notasFiltrada);
    } else {
        listarNotas(notas);
    }
}

function cargarFormMod(name){
    let notas = JSON.parse(localStorage.getItem('notas'));
    let contact = notas.find(c => c.titulo == name);
    
    document.getElementById('titulo-modal').value = contact.titulo
    document.getElementById('cont-modal').value = contact.cont
}

function modificar(){
    let titulo = document.getElementById('titulo-modal').value;
    let newCont = document.getElementById('cont-modal').value;
    if(newCont != ''){
        let notas = JSON.parse(localStorage.getItem('notas'));
        let index = notas.findIndex(c => c.titulo == titulo);
        notas[index].cont = newCont;
        localStorage.setItem('notas', JSON.stringify(notas));
        alert('Contenido actualizado');
        $('#editModal').modal('toggle');
        buscar();
    } else {
        alert('Ingrese la informacion a modificar');
    }

}

